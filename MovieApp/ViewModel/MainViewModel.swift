//
//  MainViewControllerViewModel.swift
//  MovieApp
//
//  Created by Mingyong Zhu on 7/4/22.
//

import Combine
import Foundation

class MainViewModel {
    @Published private(set) var popularMovies = [Movie]()
    @Published private(set) var errorMessage: String?
    private var networkManager: NetworkManager
    private var popularMoviesPageNumber = 1

    init(networkManager: NetworkManager) {
        self.networkManager = networkManager

        let urlString = NetworkURL.popularMoviesBaseURL + String(popularMoviesPageNumber)

        networkManager.fetchMovieData(urlString: urlString) { [weak self] result in
            switch result {
            case .failure(let error):
                self?.errorMessage = error.localizedDescription

            case .success(let response):
                response.results.forEach {
                    self?.popularMovies.append($0)
                }
            }
        }
    }
}
