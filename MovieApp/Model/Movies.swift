//
//  Movies.swift
//  MovieApp
//
//  Created by Mingyong Zhu on 7/4/22.
//

import Foundation

struct Response: Codable {
    let page: Int
    let results: [Movie]
    let totalPages: Int
    let totalResults: Int

    enum CodingKeys: String, CodingKey {
        case page,results
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
}

struct Movie: Codable {
    let movieID: Int
    let title: String
    let overview: String
    let releaseDate: String?
    let posterPath: String?

    enum CodingKeys: String, CodingKey {
        case movieID = "id"
        case title,overview
        case releaseDate = "release_date"
        case posterPath = "poster_path"
    }
}
