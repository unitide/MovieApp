//
//  NetworkManager.swift
//  MovieApp
//
//  Created by Mingyong Zhu on 7/4/22.
//

import Combine
import Foundation

class NetworkManager {

    // MARK: Methods
    func fetchMovieData(urlString: String, completion: @escaping(Result<Response,NetworkError>) ->Void) {
        guard let url = URL(string: urlString) else {
            completion(.failure(.BadURL))
            return
        }

        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(.failure(.other(error)))
            }

            if let data = data {
                do {
                    let response = try JSONDecoder().decode(Response.self, from: data)
                    completion(.success(response))
                } catch let DecodingError.dataCorrupted(context) {
                    completion(.failure(.other(DecodingError.dataCorrupted(context))))
                    print(context)
                } catch let DecodingError.keyNotFound(key, context) {
                    completion(.failure(.other(DecodingError.keyNotFound(key, context))))
                    print("Key '\(key)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch let DecodingError.valueNotFound(value, context) {
                    completion(.failure(.other(DecodingError.valueNotFound(value, context))))
                    print("Value '\(value)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch let DecodingError.typeMismatch(type, context) {
                    completion(.failure(.other(DecodingError.typeMismatch(type, context))))
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch let error {
                    completion(.failure(.other(error)))
                }
            }
        }
        task.resume()
    }


}
