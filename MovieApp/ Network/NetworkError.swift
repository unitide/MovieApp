//
//  NetworkError.swift
//  MovieApp
//
//  Created by Mingyong Zhu on 7/4/22.
//

import Foundation

enum NetworkError: Error, LocalizedError {
    case BadURL
    case other(Error)

    var errorDescription: String? {
        switch self {
        case .BadURL:
            return "The URL is invalide!"

        case .other(let error):
            return error.localizedDescription
        }
    }
}
