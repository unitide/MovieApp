//
//  NetworkURL.swift
//  MovieApp
//
//  Created by Mingyong Zhu on 7/4/22.
//

import Foundation


enum NetworkURL {
    static let BaseURL = "https://api.themoviedb.org/3/"

    static let popularMoviesBaseURL = "https://api.themoviedb.org/3/movie/popular?api_key=142d48e7dd566de20e630c3104b37096&language=en-US&page="

    // https://api.themoviedb.org/3/movie/popular?api_key=<<api_key>>&language=en-US&page=1

    static let ImageBaseURL = "https://image.tmdb.org/t/p/w500"

    static let APIKey = "142d48e7dd566de20e630c3104b37096"
}
