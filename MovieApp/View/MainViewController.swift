//
//  ViewController.swift
//  MovieApp
//
//  Created by Mingyong Zhu on 7/4/22.
//

import Combine
import UIKit

class MainViewController: UIViewController {
    private var viewModel = MainViewModel(networkManager: NetworkManager())

    lazy private var movieTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    let cellID = "cellID"
    private var subscriptions = Set<AnyCancellable>()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        movieTableView.dataSource = self
        movieTableView.delegate = self
        movieTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellID)

        setupUI()
        setupDataBinding()

    }

    private func setupUI() {
        view.addSubview(movieTableView)
        movieTableView.frame = view.bounds
        movieTableView.center = view.center
    }

    private func setupDataBinding() {
        viewModel.$popularMovies
            .receive(on: RunLoop.main)
            .sink {[weak self] _ in
                self?.movieTableView.reloadData()
            }
            .store(in: &subscriptions)

        viewModel.$errorMessage
            .receive(on: RunLoop.main)
            .sink { [weak self] receivedValue in
                print(receivedValue)
            }
    }

}

extension MainViewController: UITableViewDelegate {

}

extension MainViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.popularMovies.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        cell.textLabel?.text = viewModel.popularMovies[indexPath.row].title
        return cell
    }
}
